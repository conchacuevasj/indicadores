# Indicadores

Este proyecto fue desarrollado en [Angular CLI](https://github.com/angular/angular-cli) version 11.2.18 y bootstrap 4.

Antes de iniciar el proyecto instalar las dependecias NPM con `npm install`

## Servidor de desarrollo

Ejecutar `ng serve` para el servidor de desarrollo. Navega en `http://localhost:4200/`.

