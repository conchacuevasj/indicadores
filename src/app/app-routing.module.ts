import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { VerDetalleComponent } from './components/ver-detalle/ver-detalle.component';

const routes: Routes = [
  {
    path : '',
    component : HomeComponent
  },
  {
    path : 'detalle/:codigo',
    component : VerDetalleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
