import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-ver-detalle',
  templateUrl: './ver-detalle.component.html',
  styleUrls: ['./ver-detalle.component.sass']
})

export class VerDetalleComponent implements OnInit {
  public chart: any = null
  public codigo:any
  public detalle:any
  public serie:any

  constructor(private route:ActivatedRoute, private ApiService:ApiService){
    
  }
  
  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap:any) => {
      const {params} = paramMap
      this.cargarDetalle(params.codigo)
    })
  }

  public cargarDetalle(codigo:any){
    this.ApiService.getDetalleIndicador(codigo).subscribe((resp:any) => {
      this.detalle = resp
    })
  }
  
  
}
