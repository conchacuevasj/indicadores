import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ApiService } from '../../services/api.service'


@Component({
  selector: 'app-linecharts',
  templateUrl: './linecharts.component.html',
  styleUrls: ['./linecharts.component.sass']
})
export class LinechartsComponent implements OnInit {
  public detalle:any;
  public valorConcat:Array<any> = []
  public fechaConcat:Array<any> = []
  public chartType: string = 'line'
  
  public chartDatasets: Array<any> = [
    { data: [], label: '' },
  ];
  

  public chartLabels: Array<any> = []

  public chartColors: Array<any> = [
    {
      backgroundColor: '#011c4db5',
      borderColor: '#011c4d',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
  constructor(public route:ActivatedRoute ,public ApiService:ApiService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap:any) => {
      const {params} = paramMap
      this.cargarDataChart(params.codigo)
    })
  }

  cargarDataChart(codigo:any){
    this.ApiService.getDetalleIndicador(codigo).subscribe((resp:any) => {
      this.detalle = resp
      for (let dato of this.detalle.serie){
        this.valorConcat =  this.valorConcat.concat(dato.valor)
        let fecha = formatDate(dato.fecha,'dd/MM/yyyy','en-US') 
        this.fechaConcat = this.fechaConcat.concat(fecha)         
      } 
      this.chartDatasets = [
        { data: this.valorConcat, label: this.detalle.nombre }
      ]
      this.chartLabels = this.fechaConcat
      
      console.log(this.chartLabels)
    })
  }
 
  
}
