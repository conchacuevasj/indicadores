import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  title = "Indicadores Económicos"
  public indicadores:any
  constructor(private ApiService:ApiService) {
    this.ApiService.getIndicadores().subscribe((resp:any) => {
      this.indicadores = resp
    })
  }

  ngOnInit(): void {
  }

}
