import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  _url = "https://mindicador.cl/api"
  constructor(
    private http:HttpClient
  ) { 
  }
  getIndicadores(){
      return this.http.get(this._url)
  }
  getDetalleIndicador(codigo:string){
    return this.http.get(this._url+'/'+codigo)
  }
  
}
