import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { ChartsModule, WavesModule } from 'angular-bootstrap-md'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ObjToArrayPipe } from './objToArray.pipe';
import { HomeComponent } from './components/home/home.component';
import { VerDetalleComponent } from './components/ver-detalle/ver-detalle.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LinechartsComponent } from './components/linecharts/linecharts.component';

@NgModule({
  declarations: [
    AppComponent,
    ObjToArrayPipe,
    HomeComponent,
    VerDetalleComponent,
    HeaderComponent,
    FooterComponent,
    LinechartsComponent,
    LinechartsComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpClientModule, ChartsModule, WavesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
